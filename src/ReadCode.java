import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class ReadCode {

	public static void main(String[] args) {
		Term[] termArray = createArray();
		findAmount(termArray);
	}

	public static Term[] expandTerms(Term[] termsArray) {
		Term[] copyArray = new Term[termsArray.length * 2];
		for (int i = 0; i < termsArray.length; i++)
			copyArray[i] = termsArray[i];
		return copyArray;
	}

	public static void findAmount(Term[] termArray) {
		Scanner scan = new Scanner(System.in);
		System.out.println(
				"Hello to the game.\nEnter one word, or if you want two, |. Followed by the amount of words:\n");
		String words = scan.nextLine();
		@SuppressWarnings("resource")
		Scanner s = new Scanner(words).useDelimiter(" ");
		String firstString = "";
		String secondString = "none";
		String wordAmount = "";
		String[] stringArray = new String[4];
		int i = 0;
		while (s.hasNext()) {
			stringArray[i] = s.next().split(" ")[0];
			i++;
		}
		scan.close();
		s.close();
		firstString = stringArray[0];
		for (int j = 0; j < i; j++) {
			System.out.println("# " + i + " = " + stringArray[j]);
		}
		if (stringArray[1] == "|") {
			secondString = stringArray[2];
			wordAmount = stringArray[3];
		} else {
			wordAmount = stringArray[1];
		}
		LeftistHeap<Term> theMergedWordHeap = new LeftistHeap<>();

		theMergedWordHeap.merge(createLeftistHeap(termArray, firstString));
		if (secondString != "none")
			theMergedWordHeap.merge(createLeftistHeap(termArray, secondString));
		i = 0;
		if (secondString != "none")
			System.out.println(
					"Substring: " + firstString + " | " + secondString + " count = " + Integer.valueOf(wordAmount));
		else {
			System.out.println("Substring: " + firstString + " count=" + Integer.valueOf(wordAmount));
		}
		while (i < Integer.valueOf(wordAmount)) {
			i++;
			if (theMergedWordHeap.isEmpty()) {
				System.out.println("Sorry, but the heap is empty.");
				i = Integer.valueOf(wordAmount);
			} else {
				System.out.println("The number " + i + " frequent word is " + theMergedWordHeap.deleteMax().word);
			}
		}
	}

	public static LeftistHeap<Term> createLeftistHeap(Term[] termArray, String word) {
		LeftistHeap<Term> temporaryHeap = new LeftistHeap<>();
		int firstOccurrence = firstIndexOf(termArray, word);
		int firstEnd = lastIndexOf(termArray, word);
		for (int i = firstOccurrence; i < firstEnd; i++) {
			temporaryHeap.insert(termArray[i]);
		}
		return temporaryHeap;
	}

	public static Term[] createArray() {
		Term[] arrayTerm = new Term[100];
		String filename = "src\\SortedWords.txt";
		Scanner reader;
		int skip = 4351;
		try {
			reader = new Scanner(new File(filename));
			int i = 0;
			while ((reader.hasNext())) {
					if(i == 0)
						if(skip == reader.nextInt())
							;
					String word = reader.next();
					long freq = reader.nextInt();
					Term term = new Term(word, freq);
					if (arrayTerm.length == i + 1)
						arrayTerm = expandTerms(arrayTerm);
					arrayTerm[i] = term;
					i++;
				}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return arrayTerm;
	}

	static int firstIndexOf(Term a[], String target) {
		boolean found = false;
		int i = 0;
		Term find;
		while (i < a.length && a[i] != null && !found) {
			find = a[i];
			if (find.word.indexOf(target) == 0) {
				found = true;
			} else {
				i++;
			}
		}
		return i;
	}

	// find the last location of key in a[] which matches r locations of key.
	static int lastIndexOf(Term a[], String target) {
		boolean lastOne = false;
		int i = firstIndexOf(a, target);
		Term find;
		while (!lastOne && i < a.length && a[i] != null) {
			find = a[i];
			if (find.word.indexOf(target) != 0) {
				lastOne = true;
			} else {
				i++;
			}
		}
		return i;
	}
}